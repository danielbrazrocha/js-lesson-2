let grades =
[
  {
      "nome": "Pedro",
      "turma": "A",
      "nota1": 10,
      "nota2": 7
  },
  {
      "nome": "Maria",
      "turma": "B",
      "nota1": 7,
      "nota2": 4
  },
  {
      "nome": "Jonathan",
      "turma": "C",
      "nota1": 9,
      "nota2": 9
  },
  {
    "nome": "Daniel",
    "turma": "A",
    "nota1": 5,
    "nota2": 10
},
  {
    "nome": "João",
    "turma": "C",
    "nota1": 8,
    "nota2": 7
}
];

/*
Fazer uma função que retorne os alunos com as medias mais altas de cada turma, dada uma coleção de objetos 
Resultado:
O aluno Pedro teve a media mais alta da turma A, com 8.5 
O aluno Maria teve a media mais alta da turma B, com 5.5
O aluno Jonathan teve a media mais alta da turma C, com 9
*/
let newGrades = [];
function calc() {
  newGrades = grades.map((grade) => {
    const { nome, turma, nota1, nota2 } = grade;
    mediaValue = ( nota1 + nota2 ) / 2
    return {
      nome: nome,
      turma: turma,
      nota1: nota1,
      nota2: nota2,
      media: mediaValue,
  }
  })

  
  let biggestGradeClassA = newGrades
    .filter((classItem) => classItem.turma === "A" )
    .reduce((prev, current) => {      
      return (prev.media > current.media) ? prev : current    
  });

  let biggestGradeClassB = newGrades
    .filter((classItem) => classItem.turma === "B" )
    .reduce((prev, current) => {      
      return (prev.media > current.media) ? prev : current    
  });
  
  let biggestGradeClassC = newGrades
    .filter((classItem) => classItem.turma === "C" )
    .reduce((prev, current) => {      
      return (prev.media > current.media) ? prev : current    
  });
  
  
  
  console.log(`    
    O aluno ${biggestGradeClassA.nome} teve a média mais alta da turma ${biggestGradeClassA.turma}, com ${biggestGradeClassA.media}
    O aluno ${biggestGradeClassB.nome} teve a media mais alta da turma ${biggestGradeClassB.turma}, com ${biggestGradeClassB.media}
    O aluno ${biggestGradeClassC.nome} teve a media mais alta da turma ${biggestGradeClassC.turma}, com ${biggestGradeClassC.media}
    `

  );
  
  




  

    

  
  
};
calc();
//console.log(newGrades)